﻿using ChurchillLivingBase;
using System;

namespace ApplicantSafe
{
    class Program
    {
        /// <summary>
        /// Author : Vineet Shah
        /// Created : 08/14/2018
        /// ApplicantSafe task scheduler 
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            ApplicantSafe objApplicantSafe = new ApplicantSafe();
           
            switch (args[0])
            {
                case "applicantsafe":
                    if (args[1] == "all")
                    {
                        try
                        {
                            objApplicantSafe.OrderBackgroundScreeningAPICall();
                        }
                        catch (Exception exc)
                        {
                            Logger.LogError("applicantsafe: Error Running Process - all" + exc.Message);
                               
                            Logger.SMTPLogError("applicantsafe: Error Running Process - all");
                        }
                    }
                    else if (args[1] == "pending")
                    {
                        try
                        {
                            //objApplicantSafe.PendingOrderBackgroundScreeningAPICall();
                            objApplicantSafe.StatusfromtheGatewayAPICall();
                        }
                        catch (Exception exc)
                        {
                            Logger.LogError("applicantsafe: Error Running Process - pending" + exc.Message);

                            Logger.SMTPLogError("applicantsafe: Error Running Process - pending");
                        }
                    }
                    break;
            }

           
        }
    }
}
