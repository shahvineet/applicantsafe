﻿using ChurchillLivingBase;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Xml.Serialization;

namespace ApplicantSafe
{
    class ApplicantSafe
    {

        private const string successCode = "10001";
        public static string strURLBase = ConfigurationManager.AppSettings.Get("BaseUrl");

        /// <summary>
        /// Author : Vineet Shah
        /// Created : 08/14/2018
        /// This function will call Order Background screening which status is null or need to verify report ApplicantSafe
        /// </summary>
        public void OrderBackgroundScreeningAPICall()
        {
            try
            {
                string jsonStringForToken = WebAPICall("ApplicantSafe/GetTokenInfo");
                dynamic dynObjForToken = JsonConvert.DeserializeObject(jsonStringForToken);
                if (dynObjForToken.statusCode == successCode)
                {
                    string assignedSystemId;
                    string advertiserAssignedId;
                    string authorizationToken;

                    //Success Code here
                    var dataToken = dynObjForToken.data["tokenInfo"];
                    assignedSystemId = dataToken["assignedSystemId"].Value;
                    advertiserAssignedId = dataToken["advertiserAssignedId"].Value;
                    authorizationToken = dataToken["authorizationToken"].Value;

                    string jsonString = WebAPICall("ApplicantSafe/GetApplicantSafeGuests");
                    dynamic dynObj = JsonConvert.DeserializeObject(jsonString);

                    if (dynObj.statusCode == successCode)
                    {

                        List<ApplicantSafeGuest> lstGuest = dynObj.data["listApplicantSafeGuests"].ToObject<List<ApplicantSafeGuest>>();

                        //Success Code here
                        foreach (ApplicantSafeGuest objGuest in lstGuest)
                        {
                            PushOrderBackgroundScreening(objGuest, assignedSystemId, advertiserAssignedId, authorizationToken);
                        }
                    }
                    else
                    {
                        throw new Exception("Method Name : OrderBackgroundScreeningAPICall " + Convert.ToString(dynObj.statusCode) + " : " + Convert.ToString(dynObj.statusMessage));
                    }
                }
                else
                {
                    throw new Exception("Method Name : OrderBackgroundScreeningAPICall " + Convert.ToString(dynObjForToken.statusCode) + " : " + Convert.ToString(dynObjForToken.statusMessage));
                }
            }
            catch (Exception ex)
            {
                string CustomError = Logger.BuildErrorMessage(ex, Logger.PRESENTATION_LEVEL);
                Logger.LogError(CustomError);
                Logger.SMTPLogError(CustomError);
            }

        }

        /// <summary>
        /// Author : Vineet Shah
        /// Created : 08/14/2018
        /// This function will call Status from the Gateway for the Pending order
        /// </summary>
        public void StatusfromtheGatewayAPICall()
        {
            try
            {
                string jsonStringForToken = WebAPICall("ApplicantSafe/GetTokenInfo");
                dynamic dynObjForToken = JsonConvert.DeserializeObject(jsonStringForToken);
                if (dynObjForToken.statusCode == successCode)
                {
                    string assignedSystemId;
                    string advertiserAssignedId;
                    string authorizationToken;

                    //Success Code here
                    var dataToken = dynObjForToken.data["tokenInfo"];
                    assignedSystemId = dataToken["assignedSystemId"].Value;
                    advertiserAssignedId = dataToken["advertiserAssignedId"].Value;
                    authorizationToken = dataToken["authorizationToken"].Value;

                    string jsonString = WebAPICall("ApplicantSafe/GetApplicantSafeOrders");
                    dynamic dynObj = JsonConvert.DeserializeObject(jsonString);

                    if (dynObj.statusCode == successCode)
                    {

                        List<ApplicantSafeOrderStatus> lstorder = dynObj.data["listApplicantSafeOrderStatus"].ToObject<List<ApplicantSafeOrderStatus>>();

                        //Success Code here
                        foreach (ApplicantSafeOrderStatus objorder in lstorder)
                        {
                            PushOrderStatus(objorder, assignedSystemId, advertiserAssignedId, authorizationToken);
                        }
                    }
                    else
                    {
                        throw new Exception("Method Name : OrderBackgroundScreeningAPICall " + Convert.ToString(dynObj.statusCode) + " : " + Convert.ToString(dynObj.statusMessage));
                    }
                }
                else
                {
                    throw new Exception("Method Name : OrderBackgroundScreeningAPICall " + Convert.ToString(dynObjForToken.statusCode) + " : " + Convert.ToString(dynObjForToken.statusMessage));
                }
            }
            catch (Exception ex)
            {
                string CustomError = Logger.BuildErrorMessage(ex, Logger.PRESENTATION_LEVEL);
                Logger.LogError(CustomError);
                Logger.SMTPLogError(CustomError);
            }

        }

        /// <summary>
        /// Author : Vineet Shah
        /// Created : 08/14/2018
        /// This function will call Order Background screening which status is pending report ApplicantSafe
        /// </summary>
        public void PendingOrderBackgroundScreeningAPICall()
        {
            try
            {
                string jsonStringForToken = WebAPICall("ApplicantSafe/GetTokenInfo");
                dynamic dynObjForToken = JsonConvert.DeserializeObject(jsonStringForToken);
                if (dynObjForToken.statusCode == successCode)
                {
                    string assignedSystemId;
                    string advertiserAssignedId;
                    string authorizationToken;

                    //Success Code here
                    var dataToken = dynObjForToken.data["tokenInfo"];
                    assignedSystemId = dataToken["assignedSystemId"].Value;
                    advertiserAssignedId = dataToken["advertiserAssignedId"].Value;
                    authorizationToken = dataToken["authorizationToken"].Value;

                    string jsonString = WebAPICall("ApplicantSafe/GetApplicantSafeGuests?IsPending=1");
                    dynamic dynObj = JsonConvert.DeserializeObject(jsonString);

                    if (dynObj.statusCode == successCode)
                    {

                        List<ApplicantSafeGuest> lstGuest = dynObj.data["listApplicantSafeGuests"].ToObject<List<ApplicantSafeGuest>>();

                        //Success Code here
                        foreach (ApplicantSafeGuest objGuest in lstGuest)
                        {
                            PushOrderBackgroundScreening(objGuest, assignedSystemId, advertiserAssignedId, authorizationToken);
                        }
                    }
                    else
                    {
                        throw new Exception("Method Name : PendingOrderBackgroundScreeningAPICall " + Convert.ToString(dynObj.statusCode) + " : " + Convert.ToString(dynObj.statusMessage));
                    }
                }
                else
                {
                    throw new Exception("Method Name : PendingOrderBackgroundScreeningAPICall " + Convert.ToString(dynObjForToken.statusCode) + " : " + Convert.ToString(dynObjForToken.statusMessage));
                }
            }
            catch (Exception ex)
            {
                string CustomError = Logger.BuildErrorMessage(ex, Logger.PRESENTATION_LEVEL);
                Logger.LogError(CustomError);
                Logger.SMTPLogError(CustomError);
            }

        }
        public void PushOrderBackgroundScreening(ApplicantSafeGuest data, string assignedSystemId, string advertiserAssignedId, string authorizationToken)
        {
            HttpWebRequest objRequest = null;
            string sbRequest = string.Empty;
            BackgroundCheck objUpdate = GenerateOrderBackgroundScreening(data);
            XmlSerializer xml = new XmlSerializer(typeof(BackgroundCheck));
            using (StringWriterUTF8 stringwriter = new StringWriterUTF8())
            {
                xml.Serialize(stringwriter, objUpdate);
                sbRequest = Convert.ToString(stringwriter);
            }

            string strURLTo = ConfigurationManager.AppSettings.Get("PushApplicantSafeUrl");
            objRequest = (HttpWebRequest)WebRequest.Create(strURLTo);
            objRequest.Timeout = 600000;
            objRequest.Method = "POST";
            objRequest.ContentLength = sbRequest.Length;
            objRequest.ContentType = "application/xml";
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(sbRequest);
            myWriter.Flush();
            myWriter.Close();
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            if (objResponse != null)
            {
                using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                {
                    string ResponseXml = sr.ReadToEnd();
                    SendPushOrderBackgroundScreeningResponse(ResponseXml, sbRequest);

                }
            }

        }

        public void PushOrderStatus(ApplicantSafeOrderStatus data, string assignedSystemId, string advertiserAssignedId, string authorizationToken)
        {
            HttpWebRequest objRequest = null;
            string sbRequest = string.Empty;
            BackgroundCheck objUpdate = GenerateOrderStatus(data);
            XmlSerializer xml = new XmlSerializer(typeof(BackgroundCheck));
            using (StringWriterUTF8 stringwriter = new StringWriterUTF8())
            {
                xml.Serialize(stringwriter, objUpdate);
                sbRequest = Convert.ToString(stringwriter);
            }

            string strURLTo = ConfigurationManager.AppSettings.Get("PushApplicantSafeUrl");
            objRequest = (HttpWebRequest)WebRequest.Create(strURLTo);
            objRequest.Timeout = 600000;
            objRequest.Method = "POST";
            objRequest.ContentLength = sbRequest.Length;
            objRequest.ContentType = "application/xml";
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(sbRequest);
            myWriter.Flush();
            myWriter.Close();
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            if (objResponse != null)
            {
                using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                {
                    string ResponseXml = sr.ReadToEnd();
                    SendPushOrderBackgroundScreeningResponse(ResponseXml, sbRequest);

                }
            }

        }
        /// <summary>
        /// Code for get Generate Order Background Screening information 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private BackgroundCheck GenerateOrderBackgroundScreening(ApplicantSafeGuest data)
        {
            BackgroundCheck objUpdate = new BackgroundCheck();

            #region BackgroundSearchPackage

            BackgroundSearchPackage objBsp = new BackgroundSearchPackage();

            objBsp.Action = "submit";
            objBsp.Type = "DEMO";
            objBsp.ReferenceId = data.LFTENANTSID.ToString();

            #region PersonalData

            PersonalData objPersonalData = new PersonalData();

            #region PersonName
            PersonName objPersonName = new PersonName();
            objPersonName.GivenName = data.FirstName;
            objPersonName.MiddleName = data.MiddleName;
            objPersonName.FamilyName = data.LastName;
            objPersonName.FormattedName = data.FirstName + ' ' + data.LastName;
            #endregion

            #region Aliases
            //Aliases objAliases = new Aliases();
            //objAliases.PersonName = objPersonName;
            #endregion

            #region List of GovernmentId

            //List<GovernmentId> lstGovernmentId = new List<GovernmentId>();

            //GovernmentId ssn = new GovernmentId();
            //ssn.IssuingAuthority = "issuingAuthority";
            //ssn.Text = data.SSN;
            //lstGovernmentId.Add(ssn);

            ////GovernmentId ut = new GovernmentId();
            ////ut.IssuingAuthority = "DMV";
            ////ut.Jurisdiction = "UT";
            ////ut.Text = data.SSN;
            ////lstGovernmentId.Add(ut);

            #endregion

            #region DemographicDetail
            DemographicDetail objDemographicDetail = new DemographicDetail();
            objDemographicDetail.DateOfBirth = data.DOB.ToShortDateString();
            //objDemographicDetail.GovernmentId = lstGovernmentId;
            #endregion

            #region PostalAddress

            //PostalAddress objPostalAddress = new PostalAddress();

            //#region DeliveryAddress
            //DeliveryAddress objDeliveryAddress = new DeliveryAddress();
            //objDeliveryAddress.AddressLine = data.AddressNumber.Trim() + ' ' + data.AddressStreet.Trim();
            //#endregion

            //objPostalAddress.DeliveryAddress = objDeliveryAddress;
            //objPostalAddress.Municipality = data.AddressCity.Trim();
            //objPostalAddress.PostalCode = data.addressZipCode.Trim();
            //objPostalAddress.Region = data.AddressState.Trim();

            #endregion           

            //objPersonalData.Aliases = objAliases;
            objPersonalData.DemographicDetail = objDemographicDetail;
            //objPersonalData.EmailAddress = data.EMail.Trim();
            objPersonalData.PersonName = objPersonName;
            //objPersonalData.PostalAddress = objPostalAddress;
            //objPersonalData.Telephone = data.Phone;

            #endregion

            #region Screenings

            Screenings objScreenings = new Screenings();

            objScreenings.UseConfigurationDefaults = "yes";


            #region List of Screening

            //List<Screening> lstScreening = new List<Screening>();

            #region CreditScreening

            //Screening objCreditScreening = new Screening();

            #region CreditVendor
            //Vendor objCreditVendor = new Vendor();
            //objCreditVendor.Fraud = "yes";
            //objCreditVendor.Score = "yes";
            //objCreditVendor.Text = "Experian";
            #endregion

            //objCreditScreening.Vendor = objCreditVendor;
            //objCreditScreening.Type = "credit";

            #endregion

            //lstScreening.Add(objCreditScreening);

            #endregion

            //objScreenings.Screening = lstScreening;

            #endregion

            objBsp.PersonalData = objPersonalData;
            objBsp.Screenings = objScreenings;

            #endregion

            objUpdate.BackgroundSearchPackage = objBsp;

            return objUpdate;
        }

        /// <summary>
        /// Code for get Generate Order Status information 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private BackgroundCheck GenerateOrderStatus(ApplicantSafeOrderStatus data)
        {
            BackgroundCheck objUpdate = new BackgroundCheck();

 
            BackgroundSearchPackage objBsp = new BackgroundSearchPackage();

            objBsp.Action = "status";
            objBsp.OrderId = data.orderId.ToString(); 
           
            objUpdate.BackgroundSearchPackage = objBsp;

            return objUpdate;
        }

        /// <summary>
        /// Function for send push Order Background Screening response
        /// </summary>
        /// <param name="ResponseXml"></param>
        /// <param name="strRequest"></param>
        public void SendPushOrderBackgroundScreeningResponse(string responseXml, string strRequest = "")
        {
            #region DeSerialize Stream               
            BackgroundReports ur = ParseXML(responseXml);

            string OrderStatus = ur.BackgroundReportPackage.ScreeningStatus.OrderStatus.Split("x:")[1];

            #endregion DeSerialize Stream
            if (OrderStatus == "error")
            {
                string errMessage = string.Empty;
                if (string.IsNullOrEmpty(strRequest))
                {
                    errMessage = Environment.NewLine + "Error Coming From Applicant safe api in callback url" + Environment.NewLine +
                                    "Response Xml" + Environment.NewLine + responseXml + Environment.NewLine;
                }
                else
                {
                    errMessage = Environment.NewLine + "Error Coming From Applicant safe api in immediate response " + Environment.NewLine +
                                    "Request Xml : -" + Environment.NewLine + strRequest + Environment.NewLine +
                                    "Response Xml" + Environment.NewLine + responseXml + Environment.NewLine;
                }
                Logger.LogInfo(errMessage);
                Logger.SMTPLogError(errMessage);
            }
            #region Send_Response_To_WebAPI
            string methodType = string.Empty;
            if (string.IsNullOrEmpty(strRequest))
            {
                methodType = "callback";
            }
            else
            {
                methodType = "direct";
            }

            string ErrorCode = string.Empty;
            string ErrorDescription = string.Empty;
            string ReferenceId = "000";
            if (!string.IsNullOrEmpty(ur.BackgroundReportPackage.ReferenceId))
            {
                ReferenceId = ur.BackgroundReportPackage.ReferenceId;
            }

            if (ur.BackgroundReportPackage.ErrorReport != null)
            {
                ErrorCode = ur.BackgroundReportPackage.ErrorReport.ErrorCode;
                ErrorDescription = ur.BackgroundReportPackage.ErrorReport.ErrorDescription;
            }

            string request = @"{" +
                            "\"referenceId\":\"" + ReferenceId + "\"," +
                //"\"orderStatus\":\" - " + ur.BackgroundReportPackage.ScreeningStatus.OrderStatus + "\"," +
                "\"orderStatus\":\"" + OrderStatus + "\"," +
                "\"orderId\":\"" + ur.BackgroundReportPackage.OrderId + "\"," +
                "\"errorCode\":\"" + ErrorCode + "\"," +
                "\"errorDescription\":\"" + ErrorDescription + "\"," +
                "\"methodType\":\"" + methodType + "\"}";
            string jsonString = WebAPICall("ApplicantSafe/PushOrderBackgroundScreening", request);
            StringBuilder str = new StringBuilder();
            dynamic dynObj = JsonConvert.DeserializeObject(jsonString);
            if (dynObj.statusCode == successCode)
            {
                if (Convert.ToInt32(dynObj["totalCount"].Value) == 0)
                {
                    Logger.LogInfo(Environment.NewLine + "No Record Updated for" + Environment.NewLine +
                            "Response Xml : -" + Environment.NewLine +
                            responseXml + Environment.NewLine +
                            "Request Json : -" + Environment.NewLine +
                            request + Environment.NewLine);
                    Logger.SMTPLogError(Environment.NewLine + "No Record Updated for" + Environment.NewLine +
                            "Response Xml : -" + Environment.NewLine +
                            responseXml + Environment.NewLine +
                            "Request Json : -" + Environment.NewLine +
                            request + Environment.NewLine);
                }
            }
            else
            {
                throw new Exception("Method Name : SendPushAvailabilityResponse " + Convert.ToString(dynObj.statusCode) + ":" + Convert.ToString(dynObj.statusMessage));
            }
            #endregion Send_Response_To_WebAPI

        }

        /// <summary>
        /// function for parse xml
        /// </summary>
        /// <param name="ResponseXml"></param>
        /// <returns></returns>
        public BackgroundReports ParseXML(string ResponseXml)
        {
            BackgroundReports ur;
            XmlSerializer xmlS = new XmlSerializer(typeof(BackgroundReports), new XmlRootAttribute { ElementName = "BackgroundReports" });
            StringReader ms = new StringReader(ResponseXml);
            ur = (BackgroundReports)xmlS.Deserialize(ms);
            ms.Dispose();
            return ur;
        }


        #region Commons

        public class StringWriterUTF8 : StringWriter
        {
            public override Encoding Encoding
            {
                get { return Encoding.UTF8; }
            }
        }

        /// <summary>
        /// Common function for call web api
        /// </summary>
        /// <param name="APIName"></param>
        /// <param name="sbRequest"></param>
        /// <returns></returns>
        public string WebAPICall(string APIName, string sbRequest = "")
        {
            string WebApiUrl = ConfigurationManager.AppSettings.Get("WebAPIUrl") + APIName;
            WebRequest request = WebRequest.Create(WebApiUrl);
            request.ContentType = "application/json";
            request.Headers.Add("vendorId", ConfigurationManager.AppSettings.Get("VendorId"));
            request.Headers.Add("secretKey", ConfigurationManager.AppSettings.Get("SecretKey"));
            if (!string.IsNullOrEmpty(sbRequest))
            {
                request.Method = "POST";
                request.ContentLength = sbRequest.Length;
                StreamWriter myWriter = null;
                myWriter = new StreamWriter(request.GetRequestStream());
                myWriter.Write(sbRequest);
                myWriter.Flush();
                myWriter.Close();
            }
            else
            {
                request.Method = "GET";
            }
            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    return sr.ReadToEnd();
                }
            }
        }

        #endregion
    }
}
