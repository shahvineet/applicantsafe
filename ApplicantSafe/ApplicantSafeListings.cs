﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace ApplicantSafe
{
    [XmlRoot(ElementName = "PersonName")]
    public class PersonName
    {
        [XmlElement(ElementName = "GivenName")]
        public string GivenName { get; set; }
        [XmlElement(ElementName = "MiddleName")]
        public string MiddleName { get; set; }
        [XmlElement(ElementName = "FamilyName")]
        public string FamilyName { get; set; }
        [XmlElement(ElementName = "FormattedName")]
        public string FormattedName { get; set; }
    }

    [XmlRoot(ElementName = "Aliases")]
    public class Aliases
    {
        [XmlElement(ElementName = "PersonName")]
        public PersonName PersonName { get; set; }
    }

    [XmlRoot(ElementName = "GovernmentId")]
    public class GovernmentId
    {
        [XmlAttribute(AttributeName = "issuingAuthority")]
        public string IssuingAuthority { get; set; }
        [XmlText]
        public string Text { get; set; }
        [XmlAttribute(AttributeName = "jurisdiction")]
        public string Jurisdiction { get; set; }
    }

    [XmlRoot(ElementName = "DemographicDetail")]
    public class DemographicDetail
    {
        [XmlElement(ElementName = "GovernmentId")]
        public List<GovernmentId> GovernmentId { get; set; }
        [XmlElement(ElementName = "DateOfBirth")]
        public string DateOfBirth { get; set; }
    }

    [XmlRoot(ElementName = "DeliveryAddress")]
    public class DeliveryAddress
    {
        [XmlElement(ElementName = "AddressLine")]
        public string AddressLine { get; set; }
    }

    [XmlRoot(ElementName = "PostalAddress")]
    public class PostalAddress
    {
        [XmlElement(ElementName = "PostalCode")]
        public string PostalCode { get; set; }
        [XmlElement(ElementName = "Region")]
        public string Region { get; set; }
        [XmlElement(ElementName = "Municipality")]
        public string Municipality { get; set; }
        [XmlElement(ElementName = "DeliveryAddress")]
        public DeliveryAddress DeliveryAddress { get; set; }
    }

    [XmlRoot(ElementName = "PersonalData")]
    public class PersonalData
    {
        [XmlElement(ElementName = "PersonName")]
        public PersonName PersonName { get; set; }
        [XmlElement(ElementName = "Aliases")]
        public Aliases Aliases { get; set; }
        [XmlElement(ElementName = "DemographicDetail")]
        public DemographicDetail DemographicDetail { get; set; }
        [XmlElement(ElementName = "PostalAddress")]
        public PostalAddress PostalAddress { get; set; }
        [XmlElement(ElementName = "EmailAddress")]
        public string EmailAddress { get; set; }
        [XmlElement(ElementName = "Telephone")]
        public string Telephone { get; set; }
    }

    [XmlRoot(ElementName = "Vendor")]
    public class Vendor
    {
        [XmlAttribute(AttributeName = "score")]
        public string Score { get; set; }
        [XmlAttribute(AttributeName = "fraud")]
        public string Fraud { get; set; }
        [XmlText]
        public string Text { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "high_risk_fraud_alert")]
        public string High_risk_fraud_alert { get; set; }
    }

    [XmlRoot(ElementName = "Screening")]
    public class Screening
    {
        [XmlElement(ElementName = "Vendor")]
        public Vendor Vendor { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "qualifier")]
        public string Qualifier { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlElement(ElementName = "Region")]
        public string Region { get; set; }
        [XmlElement(ElementName = "County")]
        public string County { get; set; }
        [XmlElement(ElementName = "District")]
        public string District { get; set; }
        [XmlElement(ElementName = "Contact")]
        public Contact Contact { get; set; }
        [XmlElement(ElementName = "StartDate")]
        public string StartDate { get; set; }
        [XmlElement(ElementName = "ContactInfo")]
        public ContactInfo ContactInfo { get; set; }
        [XmlElement(ElementName = "OrganizationName")]
        public string OrganizationName { get; set; }
        [XmlElement(ElementName = "Title")]
        public string Title { get; set; }
        [XmlElement(ElementName = "JobLevelInfo")]
        public string JobLevelInfo { get; set; }
        [XmlElement(ElementName = "EndDate")]
        public string EndDate { get; set; }
        [XmlElement(ElementName = "Compensation")]
        public string Compensation { get; set; }
        [XmlElement(ElementName = "SearchLicense")]
        public SearchLicense SearchLicense { get; set; }
        [XmlElement(ElementName = "EducationHistory")]
        public EducationHistory EducationHistory { get; set; }
        [XmlElement(ElementName = "Panel")]
        public string Panel { get; set; }
        [XmlElement(ElementName = "PermissiblePurpose")]
        public string PermissiblePurpose { get; set; }
    }

    [XmlRoot(ElementName = "ContactMethod")]
    public class ContactMethod
    {
        [XmlElement(ElementName = "Telephone")]
        public string Telephone { get; set; }
    }

    [XmlRoot(ElementName = "Contact")]
    public class Contact
    {
        [XmlElement(ElementName = "PersonName")]
        public PersonName PersonName { get; set; }
        [XmlElement(ElementName = "ContactMethod")]
        public ContactMethod ContactMethod { get; set; }
        [XmlElement(ElementName = "Relationship")]
        public string Relationship { get; set; }
    }

    [XmlRoot(ElementName = "ContactInfo")]
    public class ContactInfo
    {
        [XmlElement(ElementName = "PersonName")]
        public PersonName PersonName { get; set; }
        [XmlElement(ElementName = "Telephone")]
        public string Telephone { get; set; }
        [XmlElement(ElementName = "PostalAddress")]
        public PostalAddress PostalAddress { get; set; }
    }

    [XmlRoot(ElementName = "License")]
    public class License
    {
        [XmlElement(ElementName = "LicenseNumber")]
        public string LicenseNumber { get; set; }
    }

    [XmlRoot(ElementName = "SearchLicense")]
    public class SearchLicense
    {
        [XmlElement(ElementName = "License")]
        public License License { get; set; }
    }

    [XmlRoot(ElementName = "Degree")]
    public class Degree
    {
        [XmlElement(ElementName = "DegreeName")]
        public string DegreeName { get; set; }
        [XmlAttribute(AttributeName = "degreeType")]
        public string DegreeType { get; set; }
    }

    [XmlRoot(ElementName = "StartDate")]
    public class StartDate
    {
        [XmlElement(ElementName = "StringDate")]
        public string StringDate { get; set; }
    }

    [XmlRoot(ElementName = "DatesOfAttendance")]
    public class DatesOfAttendance
    {
        [XmlElement(ElementName = "StartDate")]
        public StartDate StartDate { get; set; }
    }

    [XmlRoot(ElementName = "SchoolOrInstitution")]
    public class SchoolOrInstitution
    {
        [XmlElement(ElementName = "SchoolName")]
        public string SchoolName { get; set; }
        [XmlElement(ElementName = "PostalAddress")]
        public PostalAddress PostalAddress { get; set; }
        [XmlElement(ElementName = "Degree")]
        public Degree Degree { get; set; }
        [XmlElement(ElementName = "DatesOfAttendance")]
        public DatesOfAttendance DatesOfAttendance { get; set; }
    }

    [XmlRoot(ElementName = "EducationHistory")]
    public class EducationHistory
    {
        [XmlElement(ElementName = "SchoolOrInstitution")]
        public SchoolOrInstitution SchoolOrInstitution { get; set; }
    }

    [XmlRoot(ElementName = "AdditionalItems")]
    public class AdditionalItems
    {
        [XmlElement(ElementName = "Text")]
        public string Text { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "Screenings")]
    public class Screenings
    {
        [XmlAttribute(AttributeName = "useConfigurationDefaults")]
        public string UseConfigurationDefaults { get; set; }

        [XmlElement(ElementName = "Screening")]
        public List<Screening> Screening { get; set; }
        [XmlElement(ElementName = "AdditionalItems")]
        public List<AdditionalItems> AdditionalItems { get; set; }
    }

    [XmlRoot(ElementName = "BackgroundSearchPackage")]
    public class BackgroundSearchPackage
    {
        [XmlElement(ElementName = "ReferenceId")]
        public string ReferenceId { get; set; }
        [XmlElement(ElementName = "PersonalData")]
        public PersonalData PersonalData { get; set; }
        [XmlElement(ElementName = "Screenings")]
        public Screenings Screenings { get; set; }
        [XmlAttribute(AttributeName = "action")]
        public string Action { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        [XmlElement(ElementName = "OrderId")]
        public string OrderId { get; set; }
    }

    [XmlRoot(ElementName = "BackgroundCheck")]
    public class BackgroundCheck
    {
        [XmlElement(ElementName = "BackgroundSearchPackage")]
        public BackgroundSearchPackage BackgroundSearchPackage { get; set; }



        private string userId = ConfigurationManager.AppSettings.Get("UserId");
        private string password = ConfigurationManager.AppSettings.Get("Password");

        [XmlAttribute(AttributeName = "userId")]
        public string UserId { get => userId; set => userId = value; }

        [XmlAttribute(AttributeName = "password")]
        public string Password { get => password; set => password = value; }



    }

    //Response 
    [XmlRoot(ElementName = "ScreeningStatus")]
    public class ScreeningStatus
    {
        [XmlElement(ElementName = "OrderStatus")]
        public string OrderStatus { get; set; }
    }

    [XmlRoot(ElementName = "ErrorReport")]
    public class ErrorReport
    {
        [XmlElement(ElementName = "ErrorCode")]
        public string ErrorCode { get; set; }
        [XmlElement(ElementName = "ErrorDescription")]
        public string ErrorDescription { get; set; }
    }

    [XmlRoot(ElementName = "BackgroundReportPackage")]
    public class BackgroundReportPackage
    {
        [XmlElement(ElementName = "ReferenceId")]
        public string ReferenceId { get; set; }
        [XmlElement(ElementName = "OrderId")]
        public string OrderId { get; set; }
        [XmlElement(ElementName = "ScreeningStatus")]
        public ScreeningStatus ScreeningStatus { get; set; }
        [XmlElement(ElementName = "ErrorReport")]
        public ErrorReport ErrorReport { get; set; }

        [XmlElement(ElementName = "ReportURL")]
        public string ReportURL { get; set; }
    }

    //[XmlRoot(Namespace = "www.contoso.com", ElementName = "BackgroundReports", DataType = "string", IsNullable = true)]
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlRootAttribute("BackgroundReports", Namespace = "", IsNullable = false)]
    public class BackgroundReports
    {
        private UpdateResponseStatus statusField;

        [XmlElement(ElementName = "BackgroundReportPackage")]
        public BackgroundReportPackage BackgroundReportPackage { get; set; }
        [XmlAttribute(AttributeName = "userId")]
        public string UserId { get; set; }
        [XmlAttribute(AttributeName = "password")]
        public string Password { get; set; }
        [XmlAttribute(AttributeName = "databaseset")]
        public string Databaseset { get; set; }
    }

    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public enum UpdateResponseStatus
    {

        /// <remarks/>
        SUCCESS,

        /// <remarks/>
        ERROR,
    }

    /// <summary>
    /// Class for ApplicantSafeGuest Model
    /// </summary>
    //
    [DataContract(Name = "ApplicantSafeGuest", Namespace = "")]
    public class ApplicantSafeGuest
    {
        //Properties

        [DataMember(EmitDefaultValue = false)]
        public int rowNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string TrackingID { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string FirstName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string LastName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string MiddleName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Aliases { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string SSN { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime DOB { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AddressNumber { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AddressStreet { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AddressCity { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string AddressState { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string addressZipCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Phone { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string EMail { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string ProductName { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string Screenings { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string market { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string country { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DateTime dobsent { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int LFTENANTSID { get; set; }

    }

    /// <summary>
    /// Class for ApplicantSafe Order Status
    /// </summary>
    //
    [DataContract(Name = "ApplicantSafeOrderStatus", Namespace = "")]
    public class ApplicantSafeOrderStatus
    {
        [DataMember(EmitDefaultValue = false)]
        public int lftenantsid { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int orderId { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public string status { get; set; }
    }
}
